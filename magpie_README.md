# GITHUB MAGPIE TO BITBUCKET

> [GITHUB Magpie](https://github.com/Ouranosinc/Magpie)

[GITLAB: Test coverage parsing](https://docs.gitlab.com/ee/ci/pipelines/settings.html#test-coverage-parsing)
If you use test coverage in your code, GitLab can capture its output in the job log using a regular expression.

```yaml
Coverage > stage: test
  script:
    - make coverage
  coverage: '/^TOTAL.+?(\d+\%)$/' # pytest-cov (Python)
  artifacts:
    reports:
      cobertura: # https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreportscobertura
        - reports/coverage.xml
```

If you mean test code coverage, we currently don't support that. You'll need to output that to the logs or to an artifact to download: https://confluence.atlassian.com/bitbucket/using-artifacts-in-steps-935389074.html
